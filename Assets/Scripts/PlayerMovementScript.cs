﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour {

    private Rigidbody2D ownRigidbody;
    private const float SPEED = 250f;
    private const float JUMP_FORCE = 450f;
    private const float ROTATION_TIME = 1f;
    private Vector2 velocityVector = new Vector2(SPEED, 0);

    public static float jumpTimer = 0f;
    private const float TIME_BETWEEN_JUMPS = 1.0f;

    // Use this for initialization
    void Start () {
        ownRigidbody = GetComponent<Rigidbody2D>();
	}

    // Update is called once per frame
    void Update()
    {
        CheckJump();
    }

    void FixedUpdate () {
        MovePlayerToRight();
    }

    // Handles player movement to right
    void MovePlayerToRight()
    {
       velocityVector.y = ownRigidbody.velocity.y;
       ownRigidbody.velocity = velocityVector;
    }

    void CheckJump()
    {
        if (jumpTimer <= 0 && Time.timeSinceLevelLoad > 0.1f && 
            (Input.GetKey(KeyCode.Space) || Input.touchCount >0))
        {
            jumpTimer = TIME_BETWEEN_JUMPS;
            ownRigidbody.AddForce(Vector2.up * JUMP_FORCE, ForceMode2D.Impulse);

            //start rotation
            StartCoroutine(RotateMe(new Vector3(0,0,1) * 180, ROTATION_TIME));
        }

        jumpTimer = jumpTimer - Time.deltaTime;
    }

    //Co-routine for rotation
    IEnumerator RotateMe(Vector3 byAngles, float inTime)
    {
        Quaternion fromAngle = transform.rotation;
        fromAngle.z = fromAngle.z - 0.01f;
        fromAngle.w = fromAngle.w - 0.01f;
        Quaternion toAngle = Quaternion.Euler(transform.eulerAngles + byAngles);
        Debug.Log("toAngle = " + toAngle + " fromAngle = " + fromAngle);
        for (float t = 0f; t < 1; t += Time.deltaTime / inTime)
        {
            transform.rotation = Quaternion.Lerp(fromAngle, toAngle, t);
            yield return null;
        }

        transform.rotation = toAngle;
        yield return null;
    }

}
