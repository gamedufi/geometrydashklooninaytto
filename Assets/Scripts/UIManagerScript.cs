﻿using UnityEngine;
using System.Collections;

public class UIManagerScript : MonoBehaviour {

    private GameObject panel;

	void Start () {
        panel = GameObject.Find("Panel");
        TogglePause();
	}

    void TogglePause()
    {
        if (Time.timeScale > 0)
        {
            Time.timeScale = 0;
        } else
        {
            Time.timeScale = 1;
        }
    }

    public void OnStartButtonPressed()
    {
        panel.SetActive(false);
        TogglePause();
    }

}
