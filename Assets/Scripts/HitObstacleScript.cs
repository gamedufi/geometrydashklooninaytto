﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class HitObstacleScript : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name=="Player")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

}
