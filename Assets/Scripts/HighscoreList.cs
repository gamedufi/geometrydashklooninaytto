﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighscoreList : MonoBehaviour {

	public HighScore[] HighscoreArray;

	private Text ScoreListText;
    private Text NameListText;

    private int currentScore = 9500;
    private string currentPlayerName;

    // Use this for initialization
    void Start () {
        ScoreListText = GameObject.Find("ScoreListText").GetComponent<Text>();
        NameListText = GameObject.Find("NameListText").GetComponent<Text>();

        HighscoreArray = new HighScore[10];

		HighScore scoredata1 = new HighScore ();
		scoredata1.Score = 10000;
		scoredata1.PlayerName = "Master King Best jne";
		HighscoreArray [0] = scoredata1;

		HighScore scoredata2 = new HighScore ();
		scoredata2.Score = 9000;
		scoredata2.PlayerName = "Not as good";
		HighscoreArray [1] = scoredata2;
		HighscoreArray [2] = scoredata2;
		HighscoreArray [3] = scoredata2;


		//implement list UI text change
        FillUIList ();
	}

    public void OnButtonPressed()
    {
        //1. read name value from input field
        GameObject inputField = GameObject.Find("InputText");
        string name = inputField.GetComponent<Text>().text;

        //2. update score & name to correct list positions
        HighScore currentHighScore = new HighScore();
        currentHighScore.Score = currentScore;
        currentHighScore.PlayerName = name;

        HighscoreArray[9] = currentHighScore;

        //3. update list values into screen
        FillUIList();

        //4. close panel: find panel --> setActive(false)
        GameObject panel = GameObject.Find("InputPanel");
        panel.SetActive(false);
    }

	private void FillUIList() {
        ScoreListText.text = "";
        NameListText.text = "";

        for (int i = 0 ; i < HighscoreArray.Length; i++) {
			if (HighscoreArray [i] != null) {
                ScoreListText.text = ScoreListText.text
                + HighscoreArray[i].Score + "\n";

                NameListText.text = NameListText.text
                + HighscoreArray[i].PlayerName + "\n";
            }
		}
	}

	
}
