﻿using UnityEngine;
using System.Collections;

public class CameraMovementScript : MonoBehaviour {

    private GameObject player;
    
    void Start()
    {
        player = GameObject.Find("Player");
    }

    void Update()
    {
        MoveCameraToPlayerPosition();
    }

    // Handles camera movement to right
    void MoveCameraToPlayerPosition()
    {
        transform.position = new Vector3(player.transform.position.x + 300, 0, -10);
    }
}
