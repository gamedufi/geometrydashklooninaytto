﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

    private Text scoreText;
    private int score;

	// Use this for initialization
	void Start () {
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        score = (int) (Time.timeSinceLevelLoad * 15);
        scoreText.text = "" + score;        
	}
}
