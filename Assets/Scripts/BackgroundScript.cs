﻿using UnityEngine;
using System.Collections;

public class BackgroundScript : MonoBehaviour {

    private const float TILE_WIDTH = 2000;
    private GameObject backgroundPrefab;
    private const int NUMBER_OF_TILES = 40;

    private GameObject[] tiletable = new GameObject[NUMBER_OF_TILES];

    private float red = 1f;
    private float green = 0.1f;
    private float blue = 0.6f;

    public float redPeriod = 1f;
    public float redAmplitude = 1f;

    public float greenPeriod = 2f;
    public float greenAmplitude = 1f;

    public float bluePeriod = 3f;
    public float blueAmplitude = 5f;


    // Use this for initialization
    void Start () {
        //Generate background for the level
        backgroundPrefab = Resources.Load("Tiling") as GameObject;
        Vector2 position = new Vector2(0, 350);
        for (int i=0; i < NUMBER_OF_TILES; i++)
        {
            GameObject tile = Instantiate(backgroundPrefab, position, Quaternion.identity) as GameObject;
            tiletable[i] = tile;
            tile.transform.parent = gameObject.transform;
            position = position + new Vector2(TILE_WIDTH, 0);
        }

        
        

    }
	
	// Update is called once per frame
	void Update () {
        MoveBackgroundToRight();

        float theta = Time.timeSinceLevelLoad / redPeriod;
        red = redAmplitude * Mathf.Sin(theta);

        theta = Time.timeSinceLevelLoad / greenPeriod;
        red = greenAmplitude * Mathf.Sin(theta);

        theta = Time.timeSinceLevelLoad / bluePeriod;
        blue = blueAmplitude * Mathf.Sin(theta);


        

        for (int i = 0; i < NUMBER_OF_TILES; i++)
        {
            SpriteRenderer renderer = tiletable[i].GetComponent<SpriteRenderer>();
            renderer.color = new Color(red, green, blue, 1f);
        }

    }

    void MoveBackgroundToRight()
    {
        transform.Translate(new Vector3(150f,0,0) * Time.deltaTime);
    }

}
