﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressScript : MonoBehaviour {

    private float startX;
    private float goalX;
    private float fullDistance;

    Image progressImage;

	// Use this for initialization
	void Start () {
        startX = GameObject.FindGameObjectWithTag("Player").transform.position.x;
        goalX = GameObject.FindGameObjectWithTag("Goal").transform.position.x;

        fullDistance = goalX - startX;

        progressImage = gameObject.GetComponent<Image>();
        progressImage.fillAmount = 0;
	}
	
	// Update is called once per frame
	void Update () {
        UpdateProgress();
    }

    void UpdateProgress()
    {
        float currentX = GameObject.FindGameObjectWithTag("Player").transform.position.x;
        float currentDistance = goalX - currentX;

        float traversedFraction = currentDistance / fullDistance;
        progressImage.fillAmount = 1 - traversedFraction;
    }
}
